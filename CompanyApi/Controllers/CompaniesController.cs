﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CompanyApi.Models;
using CompanyApi.EF.CompanyProvider;
using System.IO;
using System.Text;
using OfficeOpenXml;
using System.IO.Packaging;

namespace CompanyApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly ICompanyDataProvider _companyDataProvider;

        public CompaniesController(ICompanyDataProvider companyDataProvider)
        {
            _companyDataProvider = companyDataProvider;
        }
        // GET: api/companies
        [HttpGet]
        public IEnumerable<Company> GetCompanies()
        {
            return _companyDataProvider.GetCompanies();
        }

        // GET: api/companies/5
        [HttpGet("{id}")]
        public Company GetCompany(int id)
        {
            return _companyDataProvider.GetCompanySingle(id);
        }

        // PUT: api/companies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public IActionResult UpdateCompany(Company company)
        {
            if (ModelState.IsValid)
            {
                _companyDataProvider.UpdateCompany(company);
                return Ok(new ContentResult() { Content = "Updated success", StatusCode = 200, ContentType = "application/json" });
            }
            return BadRequest(ModelState);
        }

        // POST: api/companies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public IActionResult CreateCompany(Company company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _companyDataProvider.AddCompany(company);
            return Ok(new ContentResult() { Content = "Created success", StatusCode = 200, ContentType = "application/json" });
        }
        // DELETE: api/companies/5
        [HttpDelete("{id}")]
        public IActionResult DeleteCompany(int id)
        {
            var company = _companyDataProvider.GetCompanySingle(id);
            if (company == null)
            {
                return NotFound();
            }
            _companyDataProvider.DeleteCompany(id);
            return Ok(new ContentResult() { Content = "Deleted success", StatusCode = 200, ContentType = "application/json" });
        }
        // Import file CSV to DB
        [Route("import")]
        [HttpPost]
        public IActionResult PostImportCSVToDB(IFormFile file)
        {
            if(file == null)
            {
                return BadRequest(new ContentResult() { Content = "File upload not found!", StatusCode = 400, ContentType = "application/json" });
            }

            if (file.ContentType == "text/csv")
            {
                try
                {
                    using (var reader = new StreamReader(file.OpenReadStream()))
                    {
                        while (reader.Peek() >= 0)
                        {
                            var rowData = reader.ReadLine();
                            var fieldsData = rowData.Split(',');
                            Company company = new Company();
                            company.category_code = fieldsData[0].Trim();
                            company.name = fieldsData[1].Trim();
                            company.area_code = fieldsData[2].Trim();
                            company.currency = fieldsData[3].Trim();
                            company.text_code = fieldsData[4].Trim();
                            company.bank_code = fieldsData[5].Trim();
                            company.short_name = fieldsData[6].Trim();
                            _companyDataProvider.AddCompany(company);
                        }
                    }
                    return Ok(new ContentResult() { Content = "Import data success", StatusCode = 200, ContentType = "application/json" });
                }
                catch (Exception ex)
                {
                    return StatusCode(500, $"Internal server error: {ex}");
                }
            }
            else if (file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.ContentType == "application/vnd.ms-excel")
            {
                try
                {
                    using (var stream = new MemoryStream())
                    {
                        file.CopyTo(stream);
                        using (var package = new ExcelPackage(stream))
                        {
                            ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                            var rowCount = worksheet.Dimension.Rows;
                            for (int row = 1; row <= rowCount; row++)
                            {
                                Company company = new Company();
                                company.category_code = worksheet.Cells[row, 1].Value.ToString().Trim();
                                company.name = worksheet.Cells[row, 2].Value.ToString().Trim();
                                company.area_code = worksheet.Cells[row, 3].Value.ToString().Trim();
                                company.currency = worksheet.Cells[row, 4].Value.ToString().Trim();
                                company.text_code = worksheet.Cells[row, 5].Value.ToString().Trim();
                                company.bank_code = worksheet.Cells[row, 6].Value.ToString().Trim();
                                company.short_name = worksheet.Cells[row, 7].Value.ToString().Trim();
                                _companyDataProvider.AddCompany(company);
                            }
                        }
                    }
                    return Ok(new ContentResult() { Content = "Import data success", StatusCode = 200, ContentType = "application/json" });
                }
                catch (Exception ex)
                {
                    return StatusCode(500, $"Internal server error: {ex}");
                }
            }
            else
            {
                return BadRequest(new ContentResult() { Content = "File không đúng định dạng", StatusCode = 400, ContentType = "application/json" });
            }

        }
    }
}
