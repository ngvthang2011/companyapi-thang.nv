﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CompanyApi.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Companies",
                columns: new[] { "id", "area_code", "bank_code", "category_code", "currency", "name", "short_name", "text_code" },
                values: new object[,]
                {
                    { 1, "100000", "1929391", "BDS", "VND", "Công ty cổ phần Vinhomes", "vinHomes", "abc" },
                    { 2, "781", "12039193", "BDS", "VND", "Công ty cổ phần tập đoàn Ecopark", "ecopark", "abc" },
                    { 3, "100000", "12313123", "BDS", "VND", "Công ty tập đoàn địa ốc NOVA", "nova", "abc" },
                    { 4, "781", "0192112", "BDS", "VND", "Công ty cổ phần tập đoàn Hưng Thịnh", "hungthinh", "abc" },
                    { 5, "100000", "123123123", "BDS", "VND", "Công ty cổ phần đầu tư Nam Long", "namlong", "abc" },
                    { 6, "781", "423432523", "IT", "VND", "TechBase Việt Nam", "techbase", "abc" },
                    { 7, "100000", "039342", "IT", "VND", "Giao Hàng tiết kiệm", "ghtk", "abc" },
                    { 8, "781", "01923134", "IT", "VND", "Sun* INC", "sun", "abc" },
                    { 9, "100000", "0192931", "IT", "VND", "Global Fashion Group", "gfg", "abc" },
                    { 10, "781", "89371631", "IT", "VND", "VNPT", "vnpt", "abc" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "id",
                keyValue: 10);
        }
    }
}
