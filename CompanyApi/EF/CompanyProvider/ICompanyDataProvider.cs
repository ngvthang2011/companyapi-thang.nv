﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CompanyApi.Models;

namespace CompanyApi.EF.CompanyProvider
{
    public interface ICompanyDataProvider
    {
        void AddCompany(Company company);
        void UpdateCompany(Company company);
        void DeleteCompany(int id);
        Company GetCompanySingle(int id);
        List<Company> GetCompanies();
    }
}
