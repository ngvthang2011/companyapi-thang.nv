﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CompanyApi.Models;
using CompanyApi.EF;

namespace CompanyApi.EF.CompanyProvider
{
    public class CompanyDataProvider : ICompanyDataProvider
    {
        private readonly CompanyDbContext _context;

        public CompanyDataProvider(CompanyDbContext context)
        {
            _context = context;
        }
        public void AddCompany(Company company)
        {
            _context.Companies.Add(company);
            _context.SaveChanges();
        }

        public void DeleteCompany(int id)
        {
            var entity = _context.Companies.FirstOrDefault(t => t.id == id);
            _context.Companies.Remove(entity);
            _context.SaveChanges();
        }

        public List<Company> GetCompanies()
        {
            return _context.Companies.ToList();
        }

        public Company GetCompanySingle(int id)
        {
            return _context.Companies.FirstOrDefault(t => t.id == id);
        }

        public void UpdateCompany(Company company)
        {
            _context.Companies.Update(company);
            _context.SaveChanges();
        }
    }
}
