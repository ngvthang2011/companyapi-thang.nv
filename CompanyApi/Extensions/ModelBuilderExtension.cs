﻿using CompanyApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyApi.Extensions
{
    public static class ModelBuilderExtension
    {
        public static void seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>().HasData(
                new Company() {id = 1, name = "Công ty cổ phần Vinhomes", category_code = "BDS", area_code = "100000", currency = "VND", text_code = "abc", bank_code = "1929391", short_name = "vinHomes"},
                new Company() {id = 2, name = "Công ty cổ phần tập đoàn Ecopark", category_code = "BDS", area_code = "781", currency = "VND", text_code = "abc", bank_code = "12039193", short_name = "ecopark" },
                new Company() {id = 3, name = "Công ty tập đoàn địa ốc NOVA", category_code = "BDS", area_code = "100000", currency = "VND", text_code = "abc", bank_code = "12313123", short_name = "nova" },
                new Company() {id = 4, name = "Công ty cổ phần tập đoàn Hưng Thịnh", category_code = "BDS", area_code = "781", currency = "VND", text_code = "abc", bank_code = "0192112", short_name = "hungthinh" },
                new Company() {id = 5, name = "Công ty cổ phần đầu tư Nam Long", category_code = "BDS", area_code = "100000", currency = "VND", text_code = "abc", bank_code = "123123123", short_name = "namlong" },
                new Company() {id = 6, name = "TechBase Việt Nam", category_code = "IT", area_code = "781", currency = "VND", text_code = "abc", bank_code = "423432523", short_name = "techbase" },
                new Company() {id = 7, name = "Giao Hàng tiết kiệm", category_code = "IT", area_code = "100000", currency = "VND", text_code = "abc", bank_code = "039342", short_name = "ghtk" },
                new Company() {id = 8, name = "Sun* INC", category_code = "IT", area_code = "781", currency = "VND", text_code = "abc", bank_code = "01923134", short_name = "sun" },
                new Company() {id = 9, name = "Global Fashion Group", category_code = "IT", area_code = "100000", currency = "VND", text_code = "abc", bank_code = "0192931", short_name = "gfg" },
                new Company() {id = 10, name = "VNPT", category_code = "IT", area_code = "781", currency = "VND", text_code = "abc", bank_code = "89371631", short_name = "vnpt" }
            );
        }
    }
}
