﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyApi.Models
{
    public class Company
    {
        public int id { get; set; }
        public string category_code { get; set; }
        public string name { get; set; }
        public string area_code { get; set; }
        public string currency { get; set; }
        public string text_code { get; set; }
        public string bank_code { get; set; }
        public string short_name { get; set; }
    }

    public class CompanyValidator : AbstractValidator<Company>
    {
        public CompanyValidator()
        {
            RuleFor(x => x.name).NotNull().WithMessage("Name không được để trống");
            RuleFor(x => x.category_code).NotNull().WithMessage("category_code không được để trống");
            RuleFor(x => x.area_code).NotNull().WithMessage("area_code không được để trống");
            RuleFor(x => x.currency).NotNull().WithMessage("currency không được để trống");
            RuleFor(x => x.text_code).NotNull().WithMessage("text_code không được để trống");
            RuleFor(x => x.bank_code).NotNull().WithMessage("bank_code không được để trống");
            RuleFor(x => x.short_name).NotNull().WithMessage("short_name không được để trống");
            //RuleFor(x => x.Name).Length(0, 10);
            //RuleFor(x => x.Email).EmailAddress();
            //RuleFor(x => x.Age).InclusiveBetween(18, 60);
        }
    }
}